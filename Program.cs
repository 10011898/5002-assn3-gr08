﻿using System;
using System.Collections.Generic;

// Question 1 has been replaced with Problem 25 from Project Euler.
// https://www.projecteuler.net/problem=25
// Here is the code for the original problem for anyone who wants it.
/*
    static void Main(string[] args)
        {
            int userInput;
            bool checkInput;

            while(checkInput)
            {
                if(int.TryParse(Console.ReadLine(), out userYear))
                {
                    if(userYear % 4 == 0)
                    {
                        Console.WriteLine($"{userYear} is a leap year!")
                        checkInput = false;
                    }
                    else
                    {
                        Console.WriteLine($"{userYear} is not a leap year.")
                        checkInput = false;
                    }
                }
                else
                {
                    Console.WriteLine("Please enter a number")
                }
                Console.ReadKey();
            }
        }
 */

namespace _5002_assn3_gr08
{
    class Program
    {
        // Method for checking whether a given value isn't higher or lower than given parameters.
        public static bool inRange(int value, int min, int max)
        {
            if(value >= min & value <= max)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // Method for checking whether a number is even or not.
        public static bool isEven(int value)
        {
            return value % 2 == 0;
        }
        // Question 2 - Marisa
        static void question2()
        {
            // Variables declared.
            int numberFrom = 1;
            int numberTo = 100;
            Random rnd = new Random();
            List<int> numbers = new List<int>();

            // Getting 100 different numbers using a Random() class, adding to 'numbers' list.
            for(int x = 0; x < 100; x++) 
            {
                int rndNumber = rnd.Next(numberFrom, numberTo + 1);
                numbers.Add(rndNumber);                       
            }
                // Printing generated numbers to screen.
                Console.WriteLine();
                Console.WriteLine("Random Numbers Generated");
                Console.WriteLine("Numbers : {0}", string.Join(", ",numbers));
                numbers.Sort();
                Console.WriteLine();
                Console.WriteLine("Even Numbers");

            // Checking through 'numbers' list for even numbers and printing to screen.
            List<int> isEvens = new List<int>();
            foreach(int num in numbers)
            {
                if (isEven(num))
                {
                    isEvens.Add(num);
                    Console.WriteLine(num);
                }
            }
        }

        // Question 3 - Jared
        static void Question_3()
        {
            var output = "";

            // 100 five letter words
            var a = new string[100] {"Bowen","Specs","Ipsus",
            "Axile","Colic","Shill","Espoo","Frona","Debit",
            "Gauve","Khama","Merca","Buroo","Selva","Chime",
            "Gimme","Ceria","Rydal","Lummy","Aaron","Skink",
            "Toxin","Mulla","Krone","Shamo","Artal","Nasho",
            "Stilt","Sorer","Jolty","Kewis","Piave","Liver",
            "Petra","Pater","Rushy","Nowls","Massy","Della",
            "Niven","Brief","Inner","Cuber","Bocci","Baden",
            "Hider","Agamy","Pinup","Thant","Augur","Rough",
            "Varas","Benue","Bijou","Juror","Stijl","Toffy",
            "Punce","Irbid","Brood","Annwn","Dotty","Carle",
            "Agist","Irina","Heugh","Manly","Caper","Reify",
            "Sarto","Trunk","Jumbo","Denis","Seder","Basel",
            "Meuni","Dymas","Ketch","Scend","Hills","Radar",
            "Tilde","Istle","Teeth","Dryer","Minot","Anise",
            "Walla","Rooty","Logos","Achab","Relic","Anger",
            "Oscar","Davie","Kyloe","Lynda","Piano","Malan",
            "Vexil"};

            Console.Clear();

            // Sort array into alphabetical order and print to screen in 10x10 grid to keep things easy to look at.
            Array.Sort(a);

            // Nested for loops to count from 1-10, 10 times. 
            // Effectively counting from 1-100 but allowing it to print to 10 lines instead of one really long line.
            for(int i = 0; i < 10; i++)
            {
                for (int x = i * 10; x < i * 10 + 10; x++)
                {
                    output += a[x] + ", ";
                }
                // Makes sure that there is a comma after each word unless its the last word. (Grammar)
                if(i < 9)
                {
                    Console.WriteLine(output);
                }
                else
                {
                Console.WriteLine(output.Substring(0,output.Length - 2));
                }
                output = "";
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Displayed in order of ascending.");
            Console.ResetColor();
            Console.ReadKey();

            // Reverse the before array and print it again in the same manner.
            Array.Reverse(a);
            for(int i = 0; i < 10; i++)
            {
                for (int x = i * 10; x < i * 10 + 10; x++)
                {
                    output += a[x] + ", ";
                }
                // Makes sure that there is a comma after each word unless its the last word. (Grammar)
                if(i < 9)
                {
                    Console.WriteLine(output);
                }
                else
                {
                Console.WriteLine(output.Substring(0,output.Length - 2));
                }
                output = "";
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Displayed in order of descending.");
        }
        static void Main(string[] args)
        {

            // Start by clearing the console screen and declaring variables.
            Console.Clear();
            var p = "";
            var program = 0;

            // Continues to run until 'exit' or '5' is entered.
            // Loads the corresponding program for the given number.
            do {
                // Draw UI for user interaction.
                Console.Clear();
                Console.WriteLine("************************************");
                Console.WriteLine("*****       Main Menu          *****");
                Console.WriteLine("************************************");
                Console.WriteLine("*****       Program \"1\":       *****");
                Console.WriteLine("*****       Program \"2\":       *****");
                Console.WriteLine("*****       Program \"3\":       *****");
                Console.WriteLine("*****       Program \"4\":       *****");
                Console.WriteLine("***** Enter \"5\" or type \"exit\" *****");
                Console.WriteLine("*****   to quit the program.   *****");
                Console.WriteLine("************************************");
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Choose a program to run:");
                Console.ResetColor();
                p = Console.ReadLine();
                bool checkChoice = int.TryParse(p, out program);

                // Breaks the do while loop if 'exit' is entered.
                // Thanks user for using application.
                if(p.ToLower() == "exit")
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Thanks for using the program!");
                    Console.ResetColor();
                    break;
                }

                // If the bool 'checkChoice' is 
                else if(checkChoice)
                {
                    switch (program)
                    {
                        case 1:
                        // Mark's code - Project Euler: Question 25. https://projecteuler.net/problem=25
                            var selection = 0;

                            // Continues to ask the user for an input until 'exit' is entered.
                            do
                            {
                                Console.Clear();
                                Console.WriteLine("Please enter a number 1 - 15!");
                                Console.WriteLine("or, type \"exit\" to exit.");
                                Console.WriteLine();
                                Console.ForegroundColor = ConsoleColor.Green;
                                var t = Console.ReadLine();
                                var exit = t;
                                Console.ResetColor();
                                bool checkInput = int.TryParse(t, out selection);
                                
                                // Check input for a number, if it isn't a number, requests correct input..
                                // If the input is higher or lower than the required parameter, lets the user know.
                                if(checkInput)
                                {
                                    // If the entered value is within 1-15, executes the code in Fibonacci.cs
                                    if(inRange(selection,1 ,15))
                                    {
                                        var fib1 = new Fibonacci(selection);
                                        Console.WriteLine(fib1.Calculate());
                                        Console.ReadKey();
                                    }
                                    // Notifies the user if their number is too low.
                                    else if(selection <= 0)
                                    {
                                        Console.WriteLine("That number is too low..");
                                        selection = 0;
                                        Console.WriteLine("Press Any Key to try again.");
                                        Console.ReadKey();
                                    }
                                    // Notifies the user if their number is too high.
                                    else
                                    {
                                        Console.WriteLine("That number is too high!");
                                        selection = 0;
                                        Console.WriteLine("Press Any Key to try again.");
                                        Console.ReadKey();
                                    }
                                }
                                // If exit is entered, breaks the do while loop and thanks user for using the program.
                                else if(exit == "exit")
                                {
                                    break;
                                }
                                // If anything other than a number is entered, requests proper input and loops back.
                                else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Invalid input requested. Please follow the guidelines..");
                                    Console.ResetColor();
                                    selection = 0;
                                    Console.WriteLine("Press Any Key to retry.");
                                    Console.ReadKey();
                                }
                            } while(selection <= 15); // Continues to run until "exit" is entered.

                            // Thanks the user for using the program.
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Thanks for using the program!");
                            Console.ResetColor();
                            Console.WriteLine("Press Any Key to return to the menu..");
                            Console.ReadKey();
                        break;

                        case 2:
                        // Marisa's code
                            // Runs the Method for Question 2.
                            question2();
                            // Thanks the user for using the program.
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Thanks for using the program!");
                            Console.ResetColor();
                            Console.WriteLine("Press Any Key to return to the menu..");
                            Console.ReadKey();
                        break;

                        case 3:
                        // Jared's code
                            // Runs the Method for Question 3.
                            Question_3();
                            // Thanks the user for using the program.
                            Console.WriteLine();
                            Console.WriteLine("Thanks for using the program!");
                            Console.ResetColor();
                            Console.ReadKey();
                        break;

                        case 4:
                        // Jack's code
                            int maximum;
                            bool checkExit = true;
                            // Check whether the user has entered a valid number and exits.
                            while(checkExit)
                            {
                                Console.Clear();
                                Console.WriteLine("Please enter a maximum number '1 - 10000' for a Fibonacci sequence.");
                                Console.ForegroundColor = ConsoleColor.Green;
                                // Reads the user's input and determines if the input is both a number and is in range of the required parameters.
                                if(int.TryParse(Console.ReadLine(), out maximum) & inRange(maximum, 1, 10000))
                                {
                                    // Creates a new instance of Inc and runs the code from Inc.cs.
                                    var Inc1 = new Inc(maximum);
                                    Console.ResetColor();
                                    Console.WriteLine(Inc1.Calculate());
                                    Console.WriteLine($"The total of these items is: {Inc1.Sum}");
                                    Console.WriteLine();
                                    // Exits the while loop.
                                    checkExit = false;
                                }
                                // Error checks for a '0' (Threw an error so I patched it.)
                                else if(maximum == 0)
                                {
                                    Console.Clear();
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("ERROR: Imaginary numbers not allowed!");
                                    Console.ResetColor();
                                    Console.WriteLine("Press Any Key to continue.");
                                    Console.ReadKey();
                                }
                                // If the input is anything but a number or a 0, requests proper input.
                                else
                                {
                                    Console.Clear();
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("ERROR: You didn't enter a valid number!");
                                    Console.ResetColor();
                                    Console.WriteLine("Press Any Key to continue.");
                                    Console.ReadKey();
                                }
                            }
                            // Thanks the user for using the program.
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Thanks for using the program!");
                            Console.ResetColor();
                            Console.WriteLine("Press Any Key to continue.");
                            Console.ReadKey();
                        break;
                        // Exits the program when 5 is entered.
                        case 5:
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Thanks for using the program!");
                            Console.ResetColor();
                        break;
                        // If anything other than the other given parameters are given, throws a generic error.
                        default:
                            program = 0;
                            Console.Clear();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"ERROR: You selected an invalid option..");
                            Console.ResetColor();
                            Console.WriteLine("Press Any Key to continue.");
                            Console.ReadKey();
                        break;
                    }
                }
                // If anything is entered other than the required programs, throws a generic error.
                else
                {
                    program = 0;
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("ERROR: You selected an invalid option..");
                    Console.ResetColor();
                    Console.WriteLine("Press Any Key to continue.");
                    Console.ReadKey();
                }
            }while(program < 5); // This is redundant since it continues to run regardless until 'exit' or '5' are entered.

            // Requests the user to press a button to exit the program.
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press Any Key to continue.");
            Console.ReadKey();
            Console.Clear();
        }
    }
}