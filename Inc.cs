namespace _5002_assn3_gr08
{
    // Question 4 - Jack
    class Inc
    {
        // Declares variables.
        int Max = 0;
        public int Sum {get;set;}
        string output = "";
        // Constructor for taking the user's max number.
        public Inc(int _max)
        {
            Max = _max;
        }
        // Calculate() method for doing the calculation of the Fibonacci sequence until
        // the user's defined number is reached.
        public string Calculate()
        {
            // Loops until the users defined number is reached.
            for(int i = 0; i < Max; i++)
            {
                // If the number is a multiple of 5 or 3, adds it to the total value and also the output string to show calculations.
                if(i % 5 == 0 | i % 3 == 0)
                {
                    Sum += i;
                    output += $"{i}, ";
                    Sum = Sum;
                }
            }
            // Returns the output string with the last ", " removed.
            return output.Substring(0, output.Length -2);
        }
    }
}