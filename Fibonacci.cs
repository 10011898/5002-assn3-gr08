namespace _5002_assn3_gr08
{
    // https://www.projecteuler.net/problem=25
    class Fibonacci
    {
        int Input = 0;
        // Constructor for taking the users input and creating a new instance of Fibonacci
        public Fibonacci(int _input)
        {
            Input = _input;
        }
        // Calculate() method for calculating all the Fibonacci numbers until the number reaches X amount of digits.
        // I discovered that it maxes at 15 digits before a double becomes x.xxxxx+E01
        public string Calculate()
        {
            double Num1 = 1;
            double Num2 = 2;
            string Output = "1, 2, ";
            
            // This bit was really confusing to code and even more confusing to explain
            // If the Length of both A and B are both less than the user's defined length, the block of code continues to run.
            while(Num1.ToString().Length <= Input | Num2.ToString().Length <= Input)
            {
                // If B is over the user's defined length, breaks the loop.
                // Otherwise, adds A to B.
                if(Num2.ToString().Length < Input)
                {
                    Num1 += Num2;
                    Output += $"{Num1}, ";
                }
                // Breaking loop.
                else {break;}

                // If a is over the user's defined length, breaks the loop.
                // Otherwise, adds B to A.
                if (Num1.ToString().Length < Input)
                {
                    Num2 += Num1;
                    Output += $"{Num2}, ";
                }
                // Breaking loop.
                else {break;}
            }
            // Outputs all the numbers and displays it as a string without the last ", "
            return Output.Substring(0, Output.Length -2) + "!";
        }
    }
}
